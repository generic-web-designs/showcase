import { NgModule, Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { SectionComponent } from "./section/section.component";

const routes: Routes = [{ path: "", component: SectionComponent }];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
