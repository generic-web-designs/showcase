import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-section",
  templateUrl: "./section.component.html",
  styleUrls: ["./section.component.scss"]
})
export class SectionComponent implements OnInit {
  mockdata = {
    title: "Showcase",
    description: "Images by Naolito. You should check them out, they're pretty good",
    feature: [
      {
        title: "Feature 1",
        description:
          "This feature is good, it is needing to be long paragraph to test if a feature can have a long paragraph. If this features discription of the feature looks weird then I know that things have to change",
        discriptionSize: "small",
        image: "assets/img/feature (1).jpg",
        backgroundColor: "lime",
        backgroundImage: "",
        padding: 10,
        textColor: "",
        extra: [
          { image: "", text: "extra 1", size: "15" },
          { image: "", text: "extra 2", size: "15" },
          { image: "", text: "extra 3", size: "15" }
        ]
      },
      {
        title: "Feature 2",
        description: "This feature is also good",
        discriptionSize: "small",
        image: "assets/img/feature (2).jpg",
        backgroundColor: "red",
        backgroundImage: "",
        padding: 10,
        textColor: ""
      },
      {
        title: "Feature 3",
        description: "This feature is also good",
        discriptionSize: "small",
        image: "assets/img/feature (3).jpg",
        backgroundColor: "orange",
        backgroundImage: "",
        padding: 10,
        textColor: "",
        extra: [
          { image: "", text: "extra 1", size: "15" },
          { image: "", text: "extra 2", size: "15" },
          { image: "", text: "extra 3", size: "15" }
        ]
      },
      {
        title: "Feature 4",
        description: "This feature is also good",
        discriptionSize: "small",
        image: "assets/img/feature (4).jpg",
        backgroundColor: "blue",
        backgroundImage: "",
        padding: 10,
        textColor: "",
        extra: [{ image: "", text: "extra 1", size: "15" }, { image: "", text: "extra 2", size: "15" }]
      }
    ],
    specs: {
      title: "Specifications",
      titleColor: "grey",
      items: [
        { name: "Product Name", Description: "Product", icon: "", color: "grey" },
        { name: "Dimensions", Description: "", icon: "", color: "grey" },
        { name: "Favorite Colour", Description: "Green", icon: "", color: "grey" },
        { name: "Water and Dust Resistant", Description: "IP54", icon: "", color: "grey" },
        { name: "Flute", Description: "Always", icon: "", color: "grey" },
        { name: "Feelings on chickens", Description: "Constant Fear", icon: "", color: "grey" },
        { name: "Images By", Description: "Naolito", icon: "", color: "grey" }
      ]
    }
  };

  constructor() {}

  ngOnInit() {}
}
